/*
    The definitions you are looking for are in another castle!
    definitions.js has been migrated to server/modules/definitions, please use that instead
*/
console.error(`
/------------------------------------------------------------------------------------/

The definitions you are looking for are in another castle!
definitions.js has been migrated to server/modules/definitions, please use that instead

/------------------------------------------------------------------------------------/
`)
